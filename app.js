var express = require("express");
var app = express();
const sql = require('mssql/msnodesqlv8')


var config = {
  connectionString: 'Driver=SQL Server;Server=.\\SQLEXPRESS;Database=Movie_Library;Trusted_Connection=true;'
};


app.get("/movies", (req, res, next) => {
  sql.connect(config, err => {
    new sql.Request().query('SELECT * From Movies', (err, result) => {
      console.log("Fetching data...");
      if(err) {
        console.log("An error has occured: "+ err); //return http code for error
        console.log(result)
      } else {
        res.json(result.recordset);
      };
    });
  });
 });

 app.get("/movies-with-genres", (req, res, next) => {
  sql.connect(config, err => {
    new sql.Request().query('EXEC GetMoviesWithGenres', (err, result) => {
      console.log("Fetching data...");
      if(err) {
        console.log("An error has occured: "+ err); //return http code for error
        console.log(result)
      } else {
        res.json(result.recordset);
      };
    });
  });
 });

app.listen(3000, () => {
 console.log("Server running on port 3000");
});